package be.multimedi.mastermind.database;

import be.multimedi.mastermind.exceptions.DatabaseException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Making data base connection
 *
 * @author Team-A
 */
public class Database {
    /**
     * Variables for url, login and password for the connection
     */
    private String url = "jdbc:mariadb://javadev-training.be:3306/javadevt_Hever4";
    private String login = "javadevt_StudHe";
    private String password = "STUDENTvj2020";

    /**
     * Constructor without parameters
     */
    public Database() {

    }

    /**
     * Constructor with parameters
     * @param url for database address
     * @param login for username
     * @param password for user password
     */
    public Database(String url, String login, String password) {
        setUrl(url);
        setLogin(login);
        setPassword(password);
    }

    /**
     * method for new connection making.
     * @return a connection type of object that contains the connection to database.
     * @throws SQLException could be thrown and need to be handling with try catch
     */
    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(getUrl(), getLogin(), getPassword());
    }

    /**
     * Getiing Url variable
     * @return String
     */
    public String getUrl() {
        return url;
    }

    /**
     * Getting username
     * @return Sting
     */
    public String getLogin() {
        return login;
    }

    /**
     * Getting User password
     * @return String
     */
    public String getPassword() {
        return password;
    }

    /**
     * Setting Url
     * @param url value for the setting Url variable.
     */
    public void setUrl(String url) {
        if(url == null) throw new DatabaseException("url cannot be null");
        this.url = url;
    }

    /**
     * Setting the username
     * @param login value for the setting the username
     */
    public void setLogin(String login) {
        if(login == null) throw new DatabaseException("login cannot be null");
        this.login = login;
    }

    /**
     * Setting the user password
     * @param password value for the setting the user password.
     */
    public void setPassword(String password) {
        if(password == null) throw new DatabaseException("password cannot be null");
        this.password = password;
    }
}