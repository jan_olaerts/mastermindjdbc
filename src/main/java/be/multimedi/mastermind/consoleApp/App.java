package be.multimedi.mastermind.consoleApp;

import be.multimedi.mastermind.*;
import be.multimedi.mastermind.consoleApp.daos.MMScoresDAO;
import be.multimedi.mastermind.consoleApp.pojos.MMLevels;
import be.multimedi.mastermind.exceptions.LevelException;
import be.multimedi.mastermind.consoleApp.pojos.MMScores;
import be.multimedi.mastermind.consoleApp.tools.ConsoleInputTool;
import be.multimedi.mastermind.consoleApp.tools.ConsolePrintTool;
import be.multimedi.mastermind.exceptions.LevelSQLException;
import be.multimedi.mastermind.exceptions.ScoreException;
import be.multimedi.mastermind.consoleApp.daos.MMLevelsDAO;

import java.sql.SQLException;
import java.util.List;

/**
 * Main application for mastermind game
 *
 * @author  Team-A
 */


public class App {
    Mastermind mastermind = new Mastermind();
    ConsoleInputTool consoleInput = new ConsoleInputTool();
    int gamesPlayed = 0;
    MMLevelsDAO mm = new MMLevelsDAO();
    MMScores scores =new MMScores();
    MMScoresDAO scoresDAO = new MMScoresDAO();
    MMLevelsDAO levelsDAO = new MMLevelsDAO();
    String name;

    public static void main(String[] args) throws LevelException, ScoreException, SQLException {
        App app = new App();
        app.startGame();
    }


    /**
     * where the game loop starts and when finished prints
     *
     */
    public void startGame() throws LevelException, ScoreException, SQLException {
        this.name = consoleInput.askUserName("Please enter your name: ");
        startMenu();

        System.out.println("Bye, " + name + "!");
    }

    /**
     * Determining the levels ,taking all levels from database and
     * Asking to user a select one
     *
     * @return int LevelId
     */

    public int determineLevel() throws SQLException {
        int level;

        do {
            List<MMLevels> allLevels = mm.getAllLevels();

            allLevels.forEach(l -> {
                String output = ConsolePrintTool.ANSI_YELLOW + l.getId() + ". " + l.getLevelName() + ConsolePrintTool.ANSI_RESET;
                if(l.getId() != allLevels.size()) System.out.print(output + " | ");
                else System.out.print(output);
            });

            System.out.println("\r");

            level = consoleInput.askUserPosIntBetweenRange("Give a level between 1 and 3:", 1, 3);
            if (level < 1 || level > 3) System.err.println("Level cannot be smaller than 1 or higher than 3.");
        } while (level < 1 || level > 3);

        return level;
    }

    /**
     * Setting the game level according to user input
     * @param levelId getting from user as which level wants to play
     * @throws LevelSQLException handling if there is a problem.
     */
    public void setLevelData(int levelId) throws LevelSQLException {
        if (levelId < 1 || levelId > 3) throw new IllegalArgumentException("levelId must be between 1 and 3.");

        mastermind.setGameLevel(mm.getLevelById(levelId));
    }

    /**
     *
     * Where the game functionally works
     * printing rules according to level
     * setting level, reset variables for new game, asking from user for guess, checking that guess and printing result
     * storing the score and pushing to the database
     */
    void runGame() throws LevelException, ScoreException, SQLException {

        beforePlay();
        long startTime = System.currentTimeMillis();
        while (!mastermind.isFinishedGame()) {
            String guess;
            if (!mastermind.getGameLevel().isAllowDoubles()) {

                do {
                    guess = consoleInput.askSpecificAmountOfPosDigitsStringBetweenRange(
                            "Make a guess: ",
                            mastermind.getGameLevel().getNumOfDigits(), mastermind.getGameLevel().getLowNumber(), mastermind.getGameLevel().getHighNumber());
                } while (!consoleInput.checkNoDoublesInString(guess));

            } else {
                guess = consoleInput.askSpecificAmountOfPosDigitsStringBetweenRange(
                        "Make a guess: ",
                        mastermind.getGameLevel().getNumOfDigits(), mastermind.getGameLevel().getLowNumber(), mastermind.getGameLevel().getHighNumber());
            }

            System.out.println("Answer: " + mastermind.checkGuess(guess, mastermind.getGameLevel()));
        }

        won(startTime);
    }

    /**
     * Does the logic before a game starts
     * @throws SQLException when something happens with the database
     */
    private void beforePlay() throws SQLException {

        setLevelData(determineLevel());
        mastermind.reset(mastermind.getGameLevel());
        ConsolePrintTool.printTitle("Welcome to Mastermind " + mastermind.getGameLevel().getLevelName() + " level!");
        scores = new MMScores();
        scores.setScoreLevelId(mastermind.getGameLevel().getId());
        scores.setPlayerName(name);
        if (!scoresDAO.getScoresByPlayerName(name).isEmpty()) {
            gamesPlayed = scoresDAO.resumeGamesPlayed(name).getGamesPlayed();
            scores.setGamesPlayed(gamesPlayed);
        } else {
            scores.setGamesPlayed(0);
        }

        System.out.println("I'm thinking of a " + mastermind.getGameLevel().getNumOfDigits() + " digit code. " +
                "Numbers starting from " + mastermind.getGameLevel().getLowNumber() + " up to " + mastermind.getGameLevel().getHighNumber() +
                ", duplicates" + (mastermind.getGameLevel().isAllowDoubles() ? " " : " not ") + "allowed.");

        System.out.println(Mastermind.FULL_VALID_GUESS + " represents a correct number guessed in the correct position.");
        System.out.println(Mastermind.SEMI_VALID_GUESS + " represents a correct number guessed in the wrong position.");
    }

    /**
     * Does the logic when the user won a game
     * @param startTime is the start time of the actual game
     */
    private void won(long startTime) {
        long endTime = System.currentTimeMillis();
        String duration = ConsolePrintTool.formatTime(endTime - startTime);
        System.out.println("Congratulations!");
        System.out.println("You guessed " + mastermind.getGuesses() + " times.");
        scores.setNumberOfGuesses(mastermind.getGuesses());
        scores.setTimePlayed(duration);
        gamesPlayed++;
        scores.setGamesPlayed(gamesPlayed);
        ConsolePrintTool.printHeading();
        System.out.println(scores);
        scoresDAO.addMMScores(scores);
    }

    /**
     * Printing the menu choices.
     */
    private void printMenu() {
        System.out.println("1. Play.");
        System.out.println("2. New Player");
        System.out.println("3. Overview of HighScores.");
        System.out.println("4. View previous scores by playerName.");
        System.out.println("5. Get top 10 for each level.");
        System.out.println("6. Exit the app.");
        ConsolePrintTool.printEnter();
    }

    /**
     * According to selection from the menu
     * starting different methods
     * @throws ScoreException handling if there is a problem.
     */
    public void startMenu() throws ScoreException, SQLException {
        int choice;
        do {
            System.out.println("Hello " + ConsolePrintTool.ANSI_YELLOW + name + ConsolePrintTool.ANSI_RESET + "!" + " Hope you are having a good day! Welcome to the starting menu," +
                    "please choose one of the following options: ");

            printMenu();

            choice = consoleInput.askUserPosIntBetweenRange("Input a number between 1 and 6", 1, 6);

            if (choice == 1) {
                do {
                    runGame();
                } while (consoleInput.askYesOrNo("Do you want to play again?(y/n): "));
                System.out.println(name + " played Mastermind " + gamesPlayed + " times.");
            }

            if (choice == 2) {
                this.name = consoleInput.askUserName("Please enter your name: ");
                 if (scoresDAO.getScoresByPlayerName(name).isEmpty()){
                     gamesPlayed = 0;
                     scores.setGamesPlayed(gamesPlayed);
                 }else if (!scoresDAO.getScoresByPlayerName(name).isEmpty()) {
                     gamesPlayed = scoresDAO.resumeGamesPlayed(name).getGamesPlayed();
                     scores.setGamesPlayed(gamesPlayed);
                 }
            }
            if (choice == 3) {

                List<MMLevels> allLevels = levelsDAO.getAllLevels();
                allLevels.forEach(l -> {
                    ConsolePrintTool.printEnter();
                    ConsolePrintTool.printHeading();
                    MMScores highScore = scoresDAO.getHighscorePerLevel(l.getId());
                    if(highScore != null) System.out.println(highScore);
                });
                ConsolePrintTool.printEnter();
            }

            if (choice == 4) {
                String name = consoleInput.askUserName("Insert name: ");

                ConsolePrintTool.printHeading();
                scoresDAO.getScoresByPlayerName(name).forEach(System.out::println);
                ConsolePrintTool.printEnter();
            }

            if (choice == 5) {

                List<MMLevels> allLevels = levelsDAO.getAllLevels();
                allLevels.forEach(l -> {
                    ConsolePrintTool.printEnter();
                    ConsolePrintTool.printHeading();
                    scoresDAO.getTenBestScoresPerLevel(l.getId()).forEach(System.out::println);
                });
                ConsolePrintTool.printEnter();
            }
        }
        while (choice != 6);
    }
}