package be.multimedi.mastermind.consoleApp.daos;

import be.multimedi.mastermind.consoleApp.pojos.MMScores;
import be.multimedi.mastermind.exceptions.ScoreException;

import java.util.List;
/** Data Access Object Interface for MMScores Table
 * @author Team-A
 * Defining the methods name which will using for DAO class
 */
public interface MMScoresDAO_Interface {

    MMScores getScoreById(int id) throws ScoreException;
    void addMMScores(MMScores score) throws ScoreException;
    void updateMMScores(MMScores score) throws ScoreException;
    List<MMScores> getScoresByPlayerName(String playerName) throws ScoreException;
    MMScores getHighscorePerLevel(int scoreLevelId) throws ScoreException;
    List<MMScores> getTenBestScoresPerLevel(int scoreLevelId) throws ScoreException;
    MMScores resumeGamesPlayed(String playerName) throws ScoreException;
}