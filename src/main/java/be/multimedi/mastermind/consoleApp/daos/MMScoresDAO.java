package be.multimedi.mastermind.consoleApp.daos;

import be.multimedi.mastermind.consoleApp.pojos.MMScores;
import be.multimedi.mastermind.database.Database;
import be.multimedi.mastermind.exceptions.ScoreException;
import be.multimedi.mastermind.exceptions.ScoreSQLException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/** Data Access Object for MMScores Table
 * @author Team-A
 * Making the connection with Database for MMscores table
 */
public class MMScoresDAO extends Database implements MMScoresDAO_Interface {

    public MMScoresDAO() {
        super();
    }

    public MMScoresDAO(String url, String login, String password) {
        super(url, login, password);
    }

    /**
     * SQL Query variables
     */
    private static final String SQL_GET_SCORES_BY_ID = "SELECT * FROM AMMScores WHERE id = ?;";
    private static final String SQL_INSERT_SCORE =
            "INSERT INTO AMMScores (playerName, numberOfGuesses, gamesPlayed, timePlayed, scoreLevelId) VALUES (?, ?, ?, ?, ?);";

    private static final String SQL_UPDATE_SCORE = "UPDATE AMMScores SET playerName = ?, numberOfGuesses = ?, gamesPlayed = ?, timePlayed = ?, scoreLevelId = ?;";
    private static final String SQL_GET_SCORES_BY_PLAYERNAME = "SELECT * FROM AMMScores WHERE playerName = ? ORDER BY scoreLevelId ,numberOfGuesses ,timePlayed ;";

    private static final String SQL_GET_HIGHSCORE_PER_LEVEL = "SELECT * FROM AMMScores WHERE scoreLevelId = ? ORDER BY numberOfGuesses,timePlayed ASC LIMIT 10;";

    private static final String SQL_GET_TEN_BEST_SCORES_PER_LEVEL = "SELECT * FROM AMMScores WHERE scoreLevelId = ? ORDER BY numberOfGuesses ,timePlayed LIMIT 10;";
    private static final String SQL_GET_GAMESPLAYED_FROM_EXISTING_PLAYERNAME = "SELECT * FROM AMMScores WHERE playerName = ? ORDER BY gamesPlayed DESC LIMIT 1;";

    /**
     * Getting Scores according to Id that giving.
     * @param id the primary key of the MMScores table
     * @return a MMScore object
     * @throws ScoreException  if there is a problem
     */
    @Override
    public MMScores getScoreById(int id) throws ScoreException {
        if (id < 0) throw new ScoreException("id cannot be negative");

        return getMmScores(id, SQL_GET_SCORES_BY_ID);
    }

    /**
     * Getting the data of the each level high scores from database MMScore table.
     * @param scoreLevelId defining the which level
     * @return a MMScore Obhect that contains the high score of that level
     * @throws ScoreException if there is a problem
     */
    @Override
    public MMScores getHighscorePerLevel(int scoreLevelId) throws ScoreException {
        if (scoreLevelId < 0) throw new ScoreException("scoreLevelId cannot be negative");

        return getMmScores(scoreLevelId, SQL_GET_HIGHSCORE_PER_LEVEL);
    }

    /**
     * Getting Scores from the database accoridng to given sql
     * @param scoreLevelId is int type that define the which MMScore will called from database
     * @param sql is statement for the gather data from data base
     * @return MMScores object
     */
    private MMScores getMmScores(int scoreLevelId, String sql) {
        try (Connection con = getConnection()) {
            try (PreparedStatement stmt = con.prepareStatement(sql)) {

                stmt.setInt(1, scoreLevelId);
                try (ResultSet rs = stmt.executeQuery()) {
                    if (rs.next()) {
                        try {
                            return readResultSet(rs);
                        } catch (ScoreException se) {
                            throw new ScoreSQLException(se);
                        }

                    } else {
                        return null;
                    }
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                throw new ScoreException(e);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            throw new ScoreException(e);
        }
    }

    /**
     * Adding the new Scores to MMScores database
     *
     * @param score object that contains the required information for database
     * @throws ScoreException if there is a problem
     */
    @Override
    public void addMMScores(MMScores score) throws ScoreException {
        alterMMScores(score, SQL_INSERT_SCORE);
    }

    /**
     * Updating the MMScores table
     * @param score object is contains the information for updating.
     * @throws ScoreException if there is a problem
     */
    @Override
    public void updateMMScores(MMScores score) throws ScoreException {
        alterMMScores(score, SQL_UPDATE_SCORE);
    }

    /**
     * Changing the specific row in the database
     * @param score is defining the which row will change
     * @param sql is the statement for the link database
     */
    private void alterMMScores(MMScores score, String sql) {
        if (score == null) throw new ScoreException("score cannot be null");

        try (Connection con = getConnection()) {
            try (PreparedStatement stmt = con.prepareStatement(sql)) {
                try {
                    setScore(stmt, score);
                    stmt.executeUpdate();
                } catch (ScoreException se) {
                    throw new ScoreSQLException(se);
                }

            } catch (SQLException e) {
                System.out.println(e.getMessage());
                throw new ScoreException(e);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            throw new ScoreException(e);
        }
    }

    /**
     * Getting the information according the Player name from the MMScores Table
     * @param playerName for selecting the specific row from MMScores Table
     * @return a list that contains the whole scores of the player name.
     * @throws ScoreException if there is a problem
     */
    @Override
    public List<MMScores> getScoresByPlayerName(String playerName) throws ScoreException {
        if (playerName == null) throw new ScoreException("playerName cannot be null");
        List<MMScores> scores = new ArrayList<>();
        try (Connection con = getConnection()) {
            try (PreparedStatement stmt = con.prepareStatement(SQL_GET_SCORES_BY_PLAYERNAME)) {
                stmt.setString(1, playerName);

                try (ResultSet rs = stmt.executeQuery()) {
                    while (rs.next()) {
                        try {
                            MMScores score = readResultSet(rs);
                            scores.add(score);
                        } catch (ScoreException se) {
                            throw new ScoreSQLException(se);
                        }
                    }
                    return scores;
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                throw new ScoreException(e);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            throw new ScoreException(e);
        }
    }

    /**
     * Gathering the 10 best scores from the database according to Level
     * @param scoreLevelId defining the which level
     * @return a List of MMScores objects which are contains the highest 10 scores.
     * @throws ScoreException if there is a problem
     */
    @Override
    public List<MMScores> getTenBestScoresPerLevel(int scoreLevelId) throws ScoreException {
        if (scoreLevelId < 0) throw new ScoreException("scoreLevelId cannot be negative");

        try (Connection con = getConnection()) {
            try (PreparedStatement stmt = con.prepareStatement(SQL_GET_TEN_BEST_SCORES_PER_LEVEL)) {
                stmt.setInt(1, scoreLevelId);
                try (ResultSet rs = stmt.executeQuery()) {
                    List<MMScores> scoreList = new ArrayList<>();
                    MMScores score;

                    while (rs.next()) {
                        try {
                            score = readResultSet(rs);
                            scoreList.add(score);
                        } catch (ScoreException se) {
                            throw new ScoreSQLException(se);
                        }
                    }
                    return scoreList;
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                throw new ScoreException(e);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            throw new ScoreException(e);
        }
    }

    /**
     * Checking according to username that entered if there is already date about it in database if there is take the gameplayed
     * and after finishing update it.
     * @param playerName entered username
     * @return MMScores that gathered from the Database
     * @throws ScoreException could be thrown than need to be handled with try-catch
     */
    @Override
    public MMScores resumeGamesPlayed(String playerName) throws ScoreException {
        if(playerName == null) throw new ScoreException("Invalid playername");
        try (Connection con = getConnection()) {
            try (PreparedStatement stmt = con.prepareStatement(SQL_GET_GAMESPLAYED_FROM_EXISTING_PLAYERNAME)) {
                stmt.setString(1, playerName);
                try (ResultSet rs = stmt.executeQuery()) {
                    if (rs.next()) {
                        try {
                            return readResultSet(rs);
                        } catch (ScoreException se) {
                            throw new ScoreSQLException(se);
                        }
                    } else {
                        return null;
                    }
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                throw new ScoreException(e);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            throw new ScoreException(e);
        }
    }


    /**
     * Getting the result set and assigning the variables according tha column name
     * @param rs is a object that contains the information from the database according to query statement.
     * @return a new MMScore object with assigned parameters.
     * @throws SQLException if there is a problem
     */

    private MMScores readResultSet(ResultSet rs) throws SQLException {
        int id = rs.getInt("id");
        String playerName = rs.getString("playerName");
        int numOfGuesses = rs.getInt("numberOfGuesses");
        int gamesPlayed = rs.getInt("gamesPlayed");
        String timesPlayed = rs.getString("timePlayed");
        int scoreLevelId = rs.getInt("scoreLevelId");

        return new MMScores(id, playerName, numOfGuesses, gamesPlayed, timesPlayed, scoreLevelId);
    }

    /**
     * Setting the SQL statement according to fields of score Object
     * @param stmt os a pre-made SQL statement
     * @param score is a object that need to be added or updated into database
     * @throws SQLException if there is a problem
     */
    private void setScore(PreparedStatement stmt, MMScores score) throws SQLException {
        stmt.setString(1, score.getPlayerName());
        stmt.setInt(2, score.getNumberOfGuesses());
        stmt.setInt(3, score.getGamesPlayed());
        stmt.setString(4, score.getTimePlayed());
        stmt.setInt(5, score.getScoreLevelId());
    }
}