package be.multimedi.mastermind.consoleApp.daos;

import be.multimedi.mastermind.consoleApp.pojos.MMLevels;
import be.multimedi.mastermind.database.Database;
import be.multimedi.mastermind.exceptions.LevelException;
import be.multimedi.mastermind.exceptions.LevelSQLException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/** Data Access Object for MMLevels Table
* @author Team-A
* Making the connection with Database for MMLevels table
 */
public class MMLevelsDAO extends Database implements MMLevelsDAO_Interface {

    public MMLevelsDAO() {
        super();
    }

    public MMLevelsDAO(String url, String login, String password) {
        super(url, login, password);
    }

    /**
     * SQL Query variables
     */
    private static final String SQL_GET_ALL_LEVELS = "SELECT * FROM AMMLevels;";
    private static final String SQL_GET_LEVEL_BY_ID = "SELECT * FROM AMMLevels WHERE id = ?;";

    private static final String SQL_ADD_LEVEL =
            "INSERT INTO AMMLevels (id, levelName, lowNumber, highNumber, numOfDigits, allowDoubles) " +
                    "VALUES (?, ?, ?, ?, ?, ?);";

    /**
     * Getting from MMLevels table the whole information
     * @return the MMLevels list of the levels
     * @throws LevelException if there is a problem with Level, caught by catch.
     * @throws LevelSQLException if there is a problem with SQL, caught by catch.
     */
    @Override
    public List<MMLevels> getAllLevels() throws LevelException, LevelSQLException {
        List<MMLevels> gameLevelsList = new ArrayList<>();

        try (Connection con = getConnection()) {
            try (Statement stmt = con.createStatement();
                 ResultSet rs = stmt.executeQuery(SQL_GET_ALL_LEVELS)){
                while(rs.next()) {
                    try {
                        MMLevels gameLevel = readResultSet(rs);
                        gameLevelsList.add(gameLevel);
                    } catch (LevelException le) {
                        throw new LevelSQLException(le);
                    }
                }
            } catch(SQLException se) {
                throw new LevelSQLException(se);
            }
        } catch(SQLException se) {
            throw new LevelSQLException(se);
        }
        return gameLevelsList;
    }

    /**
     * Gathering the data from MMLevels according the given Id
     * @param id is the level primary key in the MMLevels table
     * @return a MMLevels object which contains the specific information according the id param.
     * @throws LevelSQLException if there is a problem with Level or SQL will handled by catch.
     */
    @Override
    public MMLevels getLevelById(int id) throws LevelSQLException {
        if(id < 0) throw new LevelException("id cannot be negative");

        try(Connection con = getConnection()) {
            try(PreparedStatement stmt = con.prepareStatement(SQL_GET_LEVEL_BY_ID)) {
                stmt.setInt(1, id);
                try(ResultSet rs = stmt.executeQuery()) {
                    if(rs.next()) {
                        try {
                            return readResultSet(rs);
                        } catch(LevelException le) {
                            throw new LevelSQLException(le);
                        }
                    } else {
                        return null;
                    }
                }
            } catch (SQLException se) {
                throw new LevelSQLException(se);
        }
    } catch (SQLException se) {
            throw new LevelSQLException(se);
        }
    }

    /**
     * Adding a new Level to MMLevels table
     * @param level is a object that contains the required information for database
     * @throws LevelSQLException if there is a problem with Level or SQL will handled by catch.
     */

    public void addLevel(MMLevels level) throws LevelSQLException {
        if(level == null) throw new LevelException("level cannot be null");

        try(Connection con = getConnection()) {
            try (PreparedStatement stmt = con.prepareStatement(SQL_ADD_LEVEL)) {
                try {
                    stmt.setInt(1, level.getId());
                    stmt.setString(2, level.getLevelName());
                    stmt.setInt(3, level.getLowNumber());
                    stmt.setInt(4, level.getHighNumber());
                    stmt.setInt(5, level.getNumOfDigits());
                    stmt.setBoolean(6, level.isAllowDoubles());

                    stmt.executeUpdate();
                } catch (LevelException le) {
                    throw new LevelSQLException(le);
                }
            } catch (SQLException se) {
                throw new LevelSQLException(se);
            }
        } catch (SQLException se) {
            throw new LevelSQLException(se);
        }
    }

    /**
     * Getting the result set and assigning the variables according tha column name
     * @param rs is a object that contains the information from the database according to query statement.
     * @return a new MMLevels object with assigned parameters.
     * @throws SQLException if there is a problem with Level or SQL will handled by catch.
     */
    public MMLevels readResultSet(ResultSet rs) throws SQLException {
        int id = rs.getInt("id");
        String levelName = rs.getString("levelName");
        int numbersMin = rs.getInt("lowNumber");
        int numbersMax = rs.getInt("highNumber");
        int numOfDigits = rs.getInt("numOfDigits");
        boolean allowDoubles = rs.getBoolean("allowDoubles");

        return new MMLevels(id, levelName, numbersMin, numbersMax, numOfDigits, allowDoubles);
    }
}