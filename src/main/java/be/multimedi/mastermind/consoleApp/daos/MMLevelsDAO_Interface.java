package be.multimedi.mastermind.consoleApp.daos;

import be.multimedi.mastermind.consoleApp.pojos.MMLevels;
import be.multimedi.mastermind.exceptions.LevelException;
import be.multimedi.mastermind.exceptions.LevelSQLException;

import java.util.List;

/** Data Access Object Interface for MMLevels Table
 * @author Team-A
 * Defining the methods name which will using for DAO class
 */
public interface MMLevelsDAO_Interface {
    List<MMLevels> getAllLevels() throws LevelException, LevelSQLException;
    MMLevels getLevelById(int id) throws LevelException, LevelSQLException;
}