package be.multimedi.mastermind.consoleApp.pojos;

import be.multimedi.mastermind.exceptions.LevelException;

/** This is a class for Levels for gathering informations from MMLevels tables
 * and storing them in a this class object
 * @author Team-A
 */
public class MMLevels {


    /**
     * Variables which are the same type of the Database and same name declaration
     */
    private Integer id;
    private String levelName;
    private int lowNumber;
    private int highNumber;
    private int numOfDigits;
    private boolean allowDoubles;

    /**
     * MMlevels object constructor with parameters
     * @param id is an Int type variable
     * @param levelName is a string type variable
     * @param lowNumber is a int type variable
     * @param highNumber is a int type variable
     * @param numOfDigits is a int type variable
     * @param allowDoubles is a boolean type variable
     *
     */
    public MMLevels(Integer id, String levelName, int lowNumber, int highNumber, int numOfDigits, boolean allowDoubles) {

        setId(id);
        setLevelName(levelName);
        setHighNumber(highNumber);
        setLowNumber(lowNumber);
        setNumOfDigits(numOfDigits);
        setAllowDoubles(allowDoubles);
    }

    /**
     * getter for variable Id
     * @return int type variable which is Id.
     */
    public int getId() {
        return id;
    }


    /**
     * Setting for Id variables and before setting checking with validation if its proper or not.
     * @param id is giving number for setting the Id variable
     */
    public void setId(Integer id) {
        if(id != null && id < 1) throw new LevelException("id cannot be negative");
        this.id = id;
    }

    /**
     * Getting the level name variable
     * @return a string variable that contains levelName.
     */
    public String getLevelName() {
        return levelName;
    }

    /**
     * Setting the levelName variable as following parameter after validation.
     * @param levelName is a string for setting levelName.
     */
    public void setLevelName(String levelName) {
        if(levelName == null) throw new LevelException("levelName cannot be null");
        this.levelName = levelName;
    }

    /**
     * Method for  getting the lowNumber variable
     * @return int type variable which contains the lowNumber
     */
    public int getLowNumber() {
        return lowNumber;
    }

    /**
     * Method for setting the lowNumber variable after validations are okay.
     * @param lowNumber is the value for setting the lowNumber variable.
     */
    public void setLowNumber(int lowNumber) {
        if(lowNumber < 0) throw new LevelException("lowNumber cannot be negative");
        if(lowNumber > highNumber) throw new LevelException("lowNumber cannot be higher than highNumber");
        this.lowNumber = lowNumber;
    }

    /**
     * Method for gathering the highNumber variable's value
     * @return int type variable that contains highNumber variable's value
     */
    public int getHighNumber() {
        return highNumber;
    }

    /**
     *  Method for setting the highNumber variable after validations are okay.
     *
     * @param highNumber is the value for setting the highNumber variable.
     */
    public void setHighNumber(int highNumber) {
        if(highNumber < lowNumber) throw new LevelException("highNumber cannot be less than lowNumber");
        this.highNumber = highNumber;
    }

    /**
     * Method for gathering the value of the numOfDigits variable
     * @return int type value of numOfDigits variable
     */
    public int getNumOfDigits() {
        return numOfDigits;
    }

    /**
     * Method for setting the numOfDigits variable after validation is okay
     * @param numOfDigits is a value for setting the numOfDigits variable.
     */
    public void setNumOfDigits(int numOfDigits) {
        if(numOfDigits < 1) throw new LevelException("numOfDigits cannot be smaller than 1");
        this.numOfDigits = numOfDigits;
    }

    /**
     * Method for checking is true or false the AllowDoubles variable
     * @return boolean true or false according it value.
     */
    public boolean isAllowDoubles() {
        return allowDoubles;
    }

    /**
     * setting the boolean value to the AllowDoubles variable
     * @param allowDoubles is a value for setting AllowDoubles
     */
    public void setAllowDoubles(boolean allowDoubles) {
        this.allowDoubles = allowDoubles;
    }
}