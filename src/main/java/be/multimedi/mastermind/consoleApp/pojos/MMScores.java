package be.multimedi.mastermind.consoleApp.pojos;

import be.multimedi.mastermind.exceptions.ScoreException;

import java.util.Objects;
/** This is a class for Scores for gathering information from MMScores tables
 * and storing them in a this class object
 * @author Team-A
 */
public class MMScores {

    private Integer id;

    /**
     * Variables which are the same type of the Database and same name declaration
     */

    private String playerName;
    private int numberOfGuesses;
    private int gamesPlayed;
    private String timePlayed;
    private int scoreLevelId;
    /**
     * Constructor for MMScores object without parameters.
     */
    public MMScores() {
    }


    /**
     * MMScores object constructor with parameters
     * @param id is an int type variable
     * @param playerName is a string type variable
     * @param numberOfGuesses is an int type variable
     * @param gamesPlayed is an int type variable
     * @param timePlayed is a long type variable
     * @param scoreLevelId is an int type variable
     *
     */
    public MMScores(Integer id, String playerName, int numberOfGuesses, int gamesPlayed, String timePlayed, int scoreLevelId) {


        setId(id);
        setPlayerName(playerName);
        setNumberOfGuesses(numberOfGuesses);
        setGamesPlayed(gamesPlayed);
        setTimePlayed(timePlayed);
        setScoreLevelId(scoreLevelId);
    }

    /**
     * getter for variable Id
     *@return int type variable which is Id.
     */
    public int getId() {
        return id;
    }
    /**
     * Setting for Id variables and before setting checking with validation if its proper or not.
     * @param id is giving number for setting the Id variable
     */
    public void setId(Integer id) {
        if (id != null && id < 0) throw new ScoreException("id cannot be null or negative");
        this.id = id;
    }
    /**
     * Getting the playerName variable
     * @return a string variable that contains playerName.
     */
    public String getPlayerName() {
        return playerName;
    }
    /**
     * Setting the playerName variable as following parameter after validation.
     * @param playerName is a string for setting playerName.
     */
    public void setPlayerName(String playerName) {
        if (playerName == null || playerName.isBlank()) throw new ScoreException("playerName cannot be null or empty");
        this.playerName = playerName;
    }

    /**
     * Getter for variable numberOfGuesses
     * @return int type value of numberOfGuesses
     */
    public int getNumberOfGuesses() { return numberOfGuesses; }


    /**
     * Setting the variable numberOfDigits after checking with validation statements.
     * @param numberOfGuesses is value for the assign the variable numberOfDigits.
     */
    public void setNumberOfGuesses(int numberOfGuesses) {
        if(numberOfGuesses < 0) throw new ScoreException("numberOfGuesses cannot be null or negative");


        this.numberOfGuesses = numberOfGuesses;
    }

    /**
     * Getting the variable gamesPlayed
     *
     * @return an int type value of the variable.
     */
    public int getGamesPlayed() {
        return gamesPlayed;
    }

    /**
     * Setting the gamesPlayed variable after checking with the validation statement
     * @param gamesPlayed is a value for assign the variable gamesPlayed.
     */

    public void setGamesPlayed(int gamesPlayed) {
        if(gamesPlayed < 0) throw new ScoreException("gamesPlayed cannot be null or negative");
        this.gamesPlayed = gamesPlayed;
    }

    /**
     * Getting the value of timePlayed variable
     * @return a long value of timePlayed variable
     */
    public String getTimePlayed() {
        return timePlayed;
    }

    /**
     * Setting the variable timePlayed after checking with the validation statements
     * @param timePlayed is a value for assign the timePlayed variable.
     */
    public void setTimePlayed(String timePlayed) {
        if(timePlayed == null || timePlayed.isBlank()) throw new ScoreException("timePlayed cannot be null or negative");

        this.timePlayed = timePlayed;
    }

    /**
     * Getting value of the scoreLevelId variable
     * @return int type value of the variable
     */
    public int getScoreLevelId() {
        return scoreLevelId;
    }


    /**
     * Setting the variable scoreLevelId after checking with the validation statement.
     * @param scoreLevelId is a value for the assign the variable
     */
    public void setScoreLevelId(int scoreLevelId) {
        if(scoreLevelId < 0) throw new ScoreException("scoreLevelId cannot be null or negative");
        this.scoreLevelId = scoreLevelId;
    }

    /**
     * method used for when comparing two variables according their variable's values
     * @param o is an object that need to compare with our object
     * @return a true or false according the two object is equal or not
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MMScores mmScores = (MMScores) o;
        return id.equals(mmScores.id) &&
                numberOfGuesses == mmScores.numberOfGuesses &&
                gamesPlayed == mmScores.gamesPlayed &&
                scoreLevelId == mmScores.scoreLevelId &&
                Objects.equals(playerName, mmScores.playerName) &&
                Objects.equals(timePlayed, mmScores.timePlayed);
    }

    /**
     * getting the hash code of the object
     * @return int value of hashed object values
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, playerName, numberOfGuesses, gamesPlayed, timePlayed, scoreLevelId);
    }
/**
 * Method using for turning to the string values of object fields.
 */
    @Override
    public String toString() {
        return String.format("%10s |%9d|%8d|%10s|%6d",
                playerName, numberOfGuesses, gamesPlayed, timePlayed, scoreLevelId);
    }
}
