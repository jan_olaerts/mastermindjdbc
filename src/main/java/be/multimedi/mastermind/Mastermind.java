package be.multimedi.mastermind;

import be.multimedi.mastermind.consoleApp.pojos.MMLevels;
import be.multimedi.mastermind.consoleApp.tools.StringTool;

import java.util.Random;

/**
 * This class is the logical main class
 * Makes and handles events
 * Defining game logic, stages, answer, guess
 * Checking them and responsible of the storing them.
 * @author Team-A
 *
 */
public class Mastermind {
   /**
    * Required variables for the games
    * correct answer, wrong answer, semi correct answer, guesses, game information
    * finishes statements.
    */
   public static final String FULL_VALID_GUESS = "+";
   public static final String SEMI_VALID_GUESS = "-";
   static final String THE_GAME_FINISHED = "The game was already finished!";

   private String invalidGuessStructure;
   private int guesses;
   private String answer;
   private boolean finishedGame;
   private MMLevels gameLevel;

   /**
    * Getting the game level object
    * @return in type of MMLevels
    */
   public MMLevels getGameLevel() {
      return gameLevel;
   }

   /**
    * Setting the game level object after checking the validation ok.
    * @param gameLevel in type of MMlevels
    */
   public void setGameLevel(MMLevels gameLevel) {
      if(gameLevel == null) throw new IllegalArgumentException("gameLevel cannot be null and must be and instance of MMLevels");
      this.gameLevel = gameLevel;
   }

   /**
    * Getting the answer
    * @return a String
    */
   public String getAnswer() {
      return answer;
   }

   /**
    * Setting the answer according to given parameter after checking the validation is ok.
    * @param answer value of the setting the Answer variable
    */
   public void setAnswer(String answer) {
      if(answer == null) throw new IllegalArgumentException("answer cannot be null");

      String inputStructureRegex =
              "^[" + getGameLevel().getLowNumber() + "-" + getGameLevel().getHighNumber() + "]{" + getGameLevel().getNumOfDigits() + "}$";

      if (!answer.matches(inputStructureRegex))
         throw new IllegalArgumentException("The answer must contain " + gameLevel.getNumOfDigits() +
                 " digits, from " + gameLevel.getLowNumber() + " to " + gameLevel.getHighNumber() + "!");

      this.answer = answer;
   }

   /**
    * Checks the game is finished or not
    *
    * @return boolean true or false
    */
   public boolean isFinishedGame() {
      return finishedGame;
   }

   /**
    * Setting the boolean variable
    * @param finishedGame value for the setting the Variable (true or false)
    */
   public void setFinishedGame(boolean finishedGame) {
      this.finishedGame = finishedGame;
   }

   /**
    * Getting the guesses
    * @return in int type
    */
   public int getGuesses() {
      return guesses;
   }

   /**
    * Setting the guesses according to given value after checking it's validation ok.
    * @param guesses in int type value for the setting the guesses variable
    */
   public void setGuesses(int guesses) {
      if(guesses < 0) throw new IllegalArgumentException("guesses cannot be negative");
      this.guesses = guesses;
   }

   /**
    * Getting this variable when the answer or entry is invalid
    * @return in String type
    */
   public String getInvalidGuessStructure() {
      return invalidGuessStructure;
   }

   /**
    * Setting the variable as given value if it's validation is ok.
    * @param invalidGuessStructure is the value for the setting the variable
    */
   public void setInvalidGuessStructure(String invalidGuessStructure) {
      if(invalidGuessStructure == null) throw new IllegalArgumentException("invalidGuessStructure cannot be null");

      this.invalidGuessStructure = invalidGuessStructure;
   }

   /**
    * Resetting level. game loop and guess
    * also creating a new random answer according to given gameLevel object after it's validation ok
    * @param gameLevel MMLevels that need to be setting before
    */
   public void reset(MMLevels gameLevel) {
      if(gameLevel == null) throw new IllegalArgumentException("gameLevel cannot be null");

      finishedGame = false;
      guesses = 0;
      createRandomAnswer(gameLevel);
   }

   /**
    * Method for comparing the answer and the guess is right or wrong
    * @param guess the value of the user input
    * @param gameLevel the value of the user select gameLevel
    * @return String according to correct,semi-correct or false answer.
    */
   public String checkGuess(String guess, MMLevels gameLevel) {

      // validation
      if(guess == null) throw new IllegalArgumentException("guess cannot be null");
      guess = guess.replaceAll("( )*", "");

      int lowNum = gameLevel.getLowNumber();
      int highNum = gameLevel.getHighNumber();
      int numDigits = gameLevel.getNumOfDigits();

      String inputStructureRegex = "^[" + lowNum + "-" + highNum + "]{" + numDigits + "}$";

      if (guess.length() < getAnswer().length()
              || guess.length() > getAnswer().length()
              || guess.length() == 0
              || !guess.matches(inputStructureRegex)) {
         throw new IllegalArgumentException(
                 "Guess must contain " + numDigits + " digits between " + lowNum +" and " + highNum);
      }

      setInvalidGuessStructure("Invalid: The guess needs to be " + numDigits +
              " digits, from " + lowNum + " to " + highNum + "!");

      if (isFinishedGame()) return THE_GAME_FINISHED;

      if (guess.isBlank()) return getInvalidGuessStructure();
      if (!guess.matches(inputStructureRegex)) return getInvalidGuessStructure();

      // actually check the guess
      setGuesses(getGuesses() + 1);

      String answer = getAnswer();
      String currentGuess = guess;
      StringBuilder correctPos = new StringBuilder();
      StringBuilder notCorrectPos = new StringBuilder();

      for(int i = 0; i < answer.length(); i++) {
         // Check match on the same position
         if(answer.charAt(i) == guess.charAt(i)) {
            correctPos.append(FULL_VALID_GUESS);
            answer = StringTool.setX(answer, i);

            if(gameLevel.isAllowDoubles()) {
               currentGuess = StringTool.setX(currentGuess, i);
            }
         }
      }

      for (int i = 0; i < answer.length(); i++) {
         if(answer.charAt(i) == 'x') continue;

         // Check matches for all positions
         for (int j = 0; j < currentGuess.length(); j++) {
            if (i == j) continue;

            if (answer.charAt(i) == currentGuess.charAt(j) && currentGuess.charAt(j) != 'x') {
               notCorrectPos.append(SEMI_VALID_GUESS);
               answer = StringTool.setX(answer, i);

               break; // Breaking out of guess loop since answer is all unique chars
            }
         }
      }

      String result = correctPos.toString() + notCorrectPos.toString();
      //# check if the game is finished
      if (result.matches("[" + FULL_VALID_GUESS + "]{" + numDigits + "}"))
         finishedGame = true;

      //# return result
      return result;
   }

   /**
    * Creating the random answer according to given gameLevel object's data after checking it's validation ok.
    * @param gameLevel the value needed for the creating new answer criteria
    */
   public void createRandomAnswer(MMLevels gameLevel) {
      if(gameLevel == null) throw new IllegalArgumentException("gameLevel cannot be null");

      int lowNum = gameLevel.getLowNumber();
      int highNum = gameLevel.getHighNumber();
      int numDigits = gameLevel.getNumOfDigits();

      Random rand = new Random();
      StringBuilder sb = new StringBuilder();
      int count = 0;

      do {
         String input;

         do {
            input = Integer.toString(rand.nextInt(highNum + 1));
         } while(!input.matches("[" + lowNum + "-" + highNum + "]"));

         if (!gameLevel.isAllowDoubles() && !sb.toString().contains(input)) {
            sb.append(input);
            count++;
         }

         if (gameLevel.isAllowDoubles()) {
            sb.append(input);
            count++;
         }

      } while (count < numDigits);

      setAnswer(sb.toString());
   }
}