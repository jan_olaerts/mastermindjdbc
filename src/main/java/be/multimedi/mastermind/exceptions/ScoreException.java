package be.multimedi.mastermind.exceptions;
/**
 * Creating new class for handling the exceptions according to class
 */
public class ScoreException extends IllegalArgumentException{
    public ScoreException(String message) {
        super(message);
    }

    public ScoreException(Throwable cause) {
        super(cause);
    }
}
