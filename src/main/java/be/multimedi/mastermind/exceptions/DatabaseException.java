package be.multimedi.mastermind.exceptions;

/**
 * Creating new class for handling the exceptions according to class
 */
public class DatabaseException extends IllegalArgumentException {

    public DatabaseException(String message) {
        super(message);
    }
}