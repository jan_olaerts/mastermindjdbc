package be.multimedi.mastermind.exceptions;

import java.sql.SQLException;
/**
 * Creating new class for handling the exceptions according to class
 */
public class ScoreSQLException extends SQLException {

    public ScoreSQLException(Throwable cause) {
        super(cause);
    }

}