package be.multimedi.mastermind.exceptions;

import java.sql.SQLException;
/**
 * Creating new class for handling the exceptions according to class
 */
public class LevelSQLException extends SQLException {

    public LevelSQLException(Throwable cause) {
        super(cause);
    }

}