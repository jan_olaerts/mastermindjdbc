# dropping tables
DROP TABLE IF EXISTS `AMMScores`;
DROP TABLE IF EXISTS `AMMLevels`;

# creating tables
CREATE TABLE `AMMLevels`
(
    `id`           INT         NOT NULL AUTO_INCREMENT,
    `levelName`    VARCHAR(45) NOT NULL,
    `lowNumber`    INT         NOT NULL,
    `highNumber`   INT         NOT NULL,
    `numOfDigits`  INT         NOT NULL,
    `allowDoubles` TINYINT     NOT NULL,

    PRIMARY KEY (`id`),
    UNIQUE INDEX `id_UNIQUE` (`id` ASC)
);

CREATE TABLE `AMMScores`
(
    `id`              INT         NOT NULL AUTO_INCREMENT,
    `playerName`      VARCHAR(45) NOT NULL,
    `numberOfGuesses` INT         NOT NULL,
    `gamesPlayed`     INT         NOT NULL,
    `timePlayed`      VARCHAR(45) NOT NULL,
    `scoreLevelId`    INT         NOT NULL,

    PRIMARY KEY (`id`),
    UNIQUE INDEX `id_UNIQUE` (`id` ASC)
);