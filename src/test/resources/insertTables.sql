# inserting data into tables
INSERT INTO AMMLevels(id, levelName, lowNumber, highNumber, numOfDigits, allowDoubles) VALUES (1, 'Normal', 1, 6, 4, false);
INSERT INTO AMMLevels(id, levelName, lowNumber, highNumber, numOfDigits, allowDoubles) VALUES (2, 'Advanced', 1, 8, 6, false);
INSERT INTO AMMLevels(id, levelName, lowNumber, highNumber, numOfDigits, allowDoubles) VALUES (3, 'Expert', 0, 9, 6, true);

INSERT INTO AMMScores(id, playerName, numberOfGuesses, gamesPlayed, timePlayed, scoreLevelId) VALUES (1, 'Frederik', 5, 1, '00:01:17', 3);
INSERT INTO AMMScores(id, playerName, numberOfGuesses, gamesPlayed, timePlayed, scoreLevelId) VALUES (2, 'Jens', 10, 1, '00:02:17', 2);
INSERT INTO AMMScores(id, playerName, numberOfGuesses, gamesPlayed, timePlayed, scoreLevelId) VALUES (3, 'Koen', 15, 1, '00:01:34', 1);
INSERT INTO AMMScores(id, playerName, numberOfGuesses, gamesPlayed, timePlayed, scoreLevelId) VALUES (4, 'koen', 7, 2, '00:01:56', 3);
INSERT INTO AMMScores(id, playerName, numberOfGuesses, gamesPlayed, timePlayed, scoreLevelId) VALUES (5, 'koen', 13, 3, '00:02:41', 3);
INSERT INTO AMMScores(id, playerName, numberOfGuesses, gamesPlayed, timePlayed, scoreLevelId) VALUES (6, 'fred', 21, 1, '00:01:12', 3);
INSERT INTO AMMScores(id, playerName, numberOfGuesses, gamesPlayed, timePlayed, scoreLevelId) VALUES (7, 'fred', 12, 2, '00:00:18', 3);
INSERT INTO AMMScores(id, playerName, numberOfGuesses, gamesPlayed, timePlayed, scoreLevelId) VALUES (8, 'fred', 1, 3, '00:01:17', 3);
INSERT INTO AMMScores(id, playerName, numberOfGuesses, gamesPlayed, timePlayed, scoreLevelId) VALUES (9, 'fred', 9, 4, '00:02:45', 3);
INSERT INTO AMMScores(id, playerName, numberOfGuesses, gamesPlayed, timePlayed, scoreLevelId) VALUES (10, 'fred', 3, 5, '00:03:00', 2);

