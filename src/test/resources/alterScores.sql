# adding foreign key
ALTER TABLE `AMMScores`
    ADD CONSTRAINT `fk_scoreLevelId_AMMLevels`
        FOREIGN KEY (`scoreLevelId`)
            REFERENCES `AMMLevels` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION;