package be.multimedi.mastermind.database;

import be.multimedi.mastermind.exceptions.DatabaseException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Test Class for Database
 * @author Team-A
 *
 */
public class DatabaseTest {
    /**
     * Variables for Connection
     */
    private static final String URL = "jdbc:mariadb://javadev-training.be/javadevt_Hever11";
    private static final String LOGIN = "javadevt_StudHe";
    private static final String PASSWORD = "STUDENTvj2020";
    private Database database;

    /**
     * New Database instance creating with connection informations as default before each test runs.
     */
    @BeforeEach
    void init() {
        database = new Database(URL, LOGIN, PASSWORD);
    }

    /**
     * Test 1
     * Checking the setUrl method works fine or not
     */
    @Test
    void setUrl() {
        assertThrows(DatabaseException.class, () -> database.setUrl(null));

        database.setUrl("Hello");
        assertEquals("Hello", database.getUrl());
    }

    /**
     * Test 2
     * Checking the setLogin method is functional or not
     */
    @Test
    void setLogin() {
        assertThrows(DatabaseException.class, () -> database.setLogin(null));

        database.setLogin("Hello");
        assertEquals("Hello", database.getLogin());
    }

    /**
     * Test 3
     * Checking the setPassword method is functional or not
     */
    @Test
    void setPassword() {
        assertThrows(DatabaseException.class, () -> database.setPassword(null));

        database.setPassword("Hello");
        assertEquals("Hello", database.getPassword());
    }
}