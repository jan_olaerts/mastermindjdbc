package be.multimedi.mastermind.consoleApp.daos;

import be.multimedi.mastermind.consoleApp.pojos.MMScores;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

/**
 * Test Class for MMScores Data Access Object
 * Testing the methods according to critical points that could be cause a exception or a bug
 * @author Team-A
 */
class MMScoresDAOTest {
    /**
     * Connection to Test Database
     * Variables for connection
     */
    private static final String URL = "jdbc:mariadb://javadev-training.be/javadevt_Hever11";
    private static final String LOGIN = "javadevt_StudHe";
    private static final String PASSWORD = "STUDENTvj2020";

    static MMScoresDAO scoresDAO;
    /**
     * Before All test making to link the test database
     */
    @BeforeAll
    static void connectToTestDB() {
        scoresDAO = new MMScoresDAO(URL, LOGIN, PASSWORD);
    }
    /**
     * Before each method this code runs and rebuild the database as default for clean testing.
     * Creates tables, inserts default entries and linking Foreign Keys
     * @throws FileNotFoundException if there is not proper sql script to read then will throw a exception
     * @throws SQLException if there is a sql statement or connection error then will throw a SQLException
     */
    @BeforeEach
    void init() throws FileNotFoundException, SQLException {
        ScriptRunner scriptRunner = new ScriptRunner(scoresDAO.getConnection());
        scriptRunner.setLogWriter(null);

        BufferedReader bufferedReader = new BufferedReader(new FileReader("src/test/resources/createTables.sql"));
        scriptRunner.runScript(bufferedReader);

        bufferedReader = new BufferedReader(new FileReader("src/test/resources/insertTables.sql"));
        scriptRunner.runScript(bufferedReader);

        bufferedReader = new BufferedReader(new FileReader("src/test/resources/alterScores.sql"));
        scriptRunner.runScript(bufferedReader);
    }

    /**
     * Test 1
     *
     * Testing to getting Scores according to the Id and comparing them with our manuel given object if the results true or not.
     */
    @Test
    void testGetScoreById() {
        MMScores testScore = new MMScores(1, "Frederik", 5, 1, "00:01:17", 3);

        MMScores dbScore = scoresDAO.getScoreById(1);

        assertEquals(testScore.getId(), dbScore.getId());
        assertEquals(testScore.getPlayerName(), dbScore.getPlayerName());
        assertEquals(testScore.getNumberOfGuesses(), dbScore.getNumberOfGuesses());
        assertEquals(testScore.getGamesPlayed(), dbScore.getGamesPlayed());
        assertEquals(testScore.getTimePlayed(), dbScore.getTimePlayed());
        assertEquals(testScore.getScoreLevelId(), dbScore.getScoreLevelId());
    }

    /**
     * Test 2
     * Checking the AddMMScores method with comparing our manuel object and gathered object are equal or not
     *
     */
    @Test
    void testAddMMScores() {
        MMScores testScore = new MMScores();
        testScore.setPlayerName("testAdded");
        testScore.setNumberOfGuesses(20);
        testScore.setGamesPlayed(20);
        testScore.setTimePlayed("00:01:00");
        testScore.setScoreLevelId(2);

        scoresDAO.addMMScores(testScore);
        MMScores dbTestScore = scoresDAO.getScoreById(11);

        assertEquals(11, dbTestScore.getId());
        assertEquals("testAdded", dbTestScore.getPlayerName());
        assertEquals(20, dbTestScore.getNumberOfGuesses());
        assertEquals(20, dbTestScore.getGamesPlayed());
        assertEquals("00:01:00", dbTestScore.getTimePlayed());
        assertEquals(2, dbTestScore.getScoreLevelId());
    }

    /**
     * Test 3
     * Checking the update scores method with first given manuel scores for update
     * then gathering it and comparing them are they equal or not.
     */
    @Test
    void testUpdateMMScores() {

        MMScores updateScore = new MMScores(1, "updatedName", 100, 100, "00:01:00", 1);

        scoresDAO.updateMMScores(updateScore);
        MMScores testScore = scoresDAO.getScoreById(1);
        assertEquals(updateScore, testScore);
        assertEquals("updatedName", testScore.getPlayerName());
    }

    /**
     * Test 4
     * the getScoresByPlayerName method returns a list of Scores
     * This test is checking it according to expected size or not
     */
    @Test
    void testGetScoresByPlayerName() {
        assertEquals(3, scoresDAO.getScoresByPlayerName("koen").size());
        assertEquals(5, scoresDAO.getScoresByPlayerName("fred").size());
        assertEquals(1, scoresDAO.getScoresByPlayerName("Jens").size());
        assertNotEquals(2, scoresDAO.getScoresByPlayerName("Frederik").size());
    }

    /**
     * Test 5
     * Checking getHighScoresPerLevel method works properly or not
     */
    @Test
    void testGetHighscorePerLevel() {
        MMScores highscoreLevel1 = scoresDAO.getHighscorePerLevel(1);
        MMScores highscoreLevel2 = scoresDAO.getHighscorePerLevel(2);
        MMScores highscoreLevel3 = scoresDAO.getHighscorePerLevel(3);

        assertEquals(scoresDAO.getScoreById(3), highscoreLevel1 );
        assertEquals(scoresDAO.getScoreById(10), highscoreLevel2 );
        assertEquals(scoresDAO.getScoreById(8), highscoreLevel3 );
    }

    /**
     * Test 6
     * Checking the getting the 10 best scores according per-level
     *
     */
    @Test
    void testGetTenBestScoresPerLevel() {
        assertEquals(1, scoresDAO.getTenBestScoresPerLevel(1).size());
        assertEquals(2, scoresDAO.getTenBestScoresPerLevel(2).size());
        assertEquals(7, scoresDAO.getTenBestScoresPerLevel(3).size());

        List<MMScores> list1 = new ArrayList<>();
        list1.add(scoresDAO.getScoreById(3));

        List<MMScores> list2 = new ArrayList<>();
        list2.add(scoresDAO.getScoreById(10));
        list2.add(scoresDAO.getScoreById(2));

        List<MMScores> list3 = new ArrayList<>();
        list3.add(scoresDAO.getScoreById(8));
        list3.add(scoresDAO.getScoreById(1));
        list3.add(scoresDAO.getScoreById(4));
        list3.add(scoresDAO.getScoreById(9));
        list3.add(scoresDAO.getScoreById(7));
        list3.add(scoresDAO.getScoreById(5));
        list3.add(scoresDAO.getScoreById(6));

        assertEquals(scoresDAO.getTenBestScoresPerLevel(1), list1);
        assertEquals(scoresDAO.getTenBestScoresPerLevel(2), list2);
        assertEquals(scoresDAO.getTenBestScoresPerLevel(3), list3);
    }

    /**
     * Test 7
     * Checking the resumeGamePlayed method works well or not
     */
    @Test
    void testResumeGamesPlayed() {
        MMScores test = scoresDAO.getScoreById(10);
        assertEquals(test, scoresDAO.resumeGamesPlayed("fred"));

        MMScores test2 = scoresDAO.getScoreById(5);
        assertEquals(test2, scoresDAO.resumeGamesPlayed("koen"));
    }
}
