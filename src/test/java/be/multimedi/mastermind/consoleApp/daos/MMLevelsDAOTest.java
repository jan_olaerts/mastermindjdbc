package be.multimedi.mastermind.consoleApp.daos;

import be.multimedi.mastermind.consoleApp.pojos.MMLevels;
import be.multimedi.mastermind.exceptions.LevelSQLException;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

/**
 * Test Class for MMLevels Data Access Object
 * Testing the methods according to critical points that could be cause a exception or a bug
 * @author Team-A
 */
public class MMLevelsDAOTest {
    /**
     * Connection to Test Database
     * Variables for connection
     */
    private static final String URL = "jdbc:mariadb://javadev-training.be/javadevt_Hever11";
    private static final String LOGIN = "javadevt_StudHe";
    private static final String PASSWORD = "STUDENTvj2020";

    static MMLevelsDAO levelsDAO;

    /**
     * Before All test making to link the test database
     */
    @BeforeAll
    static void connectToTestDB() {
        levelsDAO = new MMLevelsDAO(URL, LOGIN, PASSWORD);
    }

    /**
     * Before each method this code runs and rebuild the database as default for clean testing.
     * Creates tables, inserts default entries and linking Foreign Keys
     * @throws FileNotFoundException if there is not proper sql script to read then will throw a exception
     * @throws SQLException if there is a sql statement or connection error then will throw a SQLException
     */
    @BeforeEach
    void init() throws FileNotFoundException, SQLException {

        ScriptRunner scriptRunner = new ScriptRunner(levelsDAO.getConnection());
        scriptRunner.setLogWriter(null);

        BufferedReader bufferedReader = new BufferedReader(new FileReader("src/test/resources/createTables.sql"));
        scriptRunner.runScript(bufferedReader);

        bufferedReader = new BufferedReader(new FileReader("src/test/resources/insertTables.sql"));
        scriptRunner.runScript(bufferedReader);

        bufferedReader = new BufferedReader(new FileReader("src/test/resources/alterScores.sql"));
        scriptRunner.runScript(bufferedReader);
    }

    /**
     * Test 1
     * Checking the getAllLevels method which is returns a List of the MMLevels and checking the size of it
     * @throws SQLException could be throw cause by sql connection or statements.
     */
    @Test
    void testGetAllLevels() throws SQLException {
        assertEquals(3, levelsDAO.getAllLevels().size());

        MMLevels testLevel = new MMLevels(4, "Difficult", 3, 8, 5, true);
        levelsDAO.addLevel(testLevel);
        assertEquals(4, levelsDAO.getAllLevels().size());

        MMLevels testLevel2 = new MMLevels(5, "Difficult", 2, 8, 5, true);
        levelsDAO.addLevel(testLevel2);
        assertEquals(5, levelsDAO.getAllLevels().size());
    }

    /**
     *Test 2
     * Checking the getting method by Id and comparing it with our manuel created object
     * @throws LevelSQLException could be throw cause by sql connection or statements
     */
    @Test
    void testGetLevelById() throws LevelSQLException {
        MMLevels dbLevel = levelsDAO.getLevelById(1);
        assertEquals(1, dbLevel.getId());
        assertEquals("Normal", dbLevel.getLevelName());
        assertEquals(1, dbLevel.getLowNumber());
        assertEquals(6, dbLevel.getHighNumber());
        assertEquals(4, dbLevel.getNumOfDigits());
        assertFalse(dbLevel.isAllowDoubles());

        MMLevels testLevel = new MMLevels(6, "Difficult", 6, 8, 5, true);
        levelsDAO.addLevel(testLevel);

        MMLevels dbLevel2 = levelsDAO.getLevelById(6);
        assertEquals(testLevel.getId(), dbLevel2.getId());
        assertEquals(testLevel.getLevelName(), dbLevel2.getLevelName());
        assertEquals(testLevel.getLowNumber(), dbLevel2.getLowNumber());
        assertEquals(testLevel.getHighNumber(), dbLevel2.getHighNumber());
        assertEquals(testLevel.getNumOfDigits(), dbLevel2.getNumOfDigits());
        assertEquals(testLevel.isAllowDoubles(), dbLevel2.isAllowDoubles());
    }

    /**
     * Test 3
     * Checking the adding Level method to the database
     * @throws SQLException could be throw cause by sql connection or statements
     */
    @Test
    void testAddLevel() throws SQLException {
        assertEquals(3, levelsDAO.getAllLevels().size());

        int amountOfRows = levelsDAO.getAllLevels().size();
        MMLevels levelToAdd = new MMLevels(45, "Super Difficult", 4, 9, 50, false);
        levelsDAO.addLevel(levelToAdd);
        assertEquals(amountOfRows + 1, levelsDAO.getAllLevels().size());
    }
}