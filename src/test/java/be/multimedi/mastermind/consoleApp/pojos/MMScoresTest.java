package be.multimedi.mastermind.consoleApp.pojos;

import be.multimedi.mastermind.exceptions.ScoreException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
/**
 * Test Class for MMScores
 * Testing the methods according to critical points that could be cause a exception or a bug
 * @author Team-A
 */
class MMScoresTest {
    MMScores score;
    /**
     * Creating new instance of the MMScores Object as default before each test run
     */
    @BeforeEach
    void init() {
        score = new MMScores(1, "Some person", 5, 4, "00:01:00", 2);
    }
    /**
     * Test 1
     * Checking the setId method is working properly or not
     */
    @Test
    void setId() {
        assertThrows(ScoreException.class, () -> score.setId(-1));

        score.setId(500);
        assertEquals(500, score.getId());
    }
    /**
     * Test 2
     * Checking the setPlayerName method is working properly or not

     */
    @Test
    void setPlayerName() {
        assertThrows(ScoreException.class, () -> score.setPlayerName(null));
        assertThrows(ScoreException.class, () -> score.setPlayerName(""));

        score.setPlayerName("A player");
        assertEquals("A player", score.getPlayerName());
    }

    /**
     * Test 3
     * Checking the setNumberOfGuesses method functional or not
     */
    @Test
    void setNumberOfGuesses() {
        assertThrows(ScoreException.class, () -> score.setNumberOfGuesses(-1));

        score.setNumberOfGuesses(100);
        assertEquals(100, score.getNumberOfGuesses());
    }

    /**
     * Test 4
     * Checking the setGamesPlayed method functional or not
     */
    @Test
    void setGamesPlayed() {
        assertThrows(ScoreException.class, () -> score.setGamesPlayed(-1));

        score.setGamesPlayed(250);
        assertEquals(250, score.getGamesPlayed());
    }

    /**
     * Test 5
     * Checking the setTimePlayed method functional or not
     */
    @Test
    void setTimePlayed() {
        assertThrows(ScoreException.class, () -> score.setTimePlayed(null));
        assertThrows(ScoreException.class, () -> score.setTimePlayed(null));

        score.setTimePlayed("00:01:00");
        assertEquals("00:01:00", score.getTimePlayed());
    }

    /**
     * Test 6
     * Checking the setScoreId method is functional or not
     */
    @Test
    void setScoreLevelId() {
        assertThrows(ScoreException.class, () -> score.setScoreLevelId(-1));

        score.setScoreLevelId(500);
        assertEquals(500, score.getScoreLevelId());
    }
}
