package be.multimedi.mastermind.consoleApp.pojos;

import be.multimedi.mastermind.exceptions.LevelException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test Class for MMLevels
 * Testing the methods according to critical points that could be cause a exception or a bug
 * @author Team-A
 */
class MMLevelsTest {
    MMLevels gameLevel;

    /**
     * Creating new instance of the MMLevels Object as default before each test run
     */
    @BeforeEach
    void init() {
        gameLevel = new MMLevels(1, "normal", 1, 6, 4, false);
    }

    /**
     * Test 1
     * Checking the setId method is working properly or not
     */
    @Test
    void setId() {
        assertThrows(LevelException.class, () -> gameLevel.setId(-1));
        gameLevel.setId(500);
        assertEquals(500, gameLevel.getId());
    }

    /**
     * Test 2
     * Checking the setLevelName method is working properly or not
     *
     */
    @Test
    void setLevelName() {
        assertThrows(LevelException.class, () -> gameLevel.setLevelName(null));

        gameLevel.setLevelName("The best level");
        assertEquals("The best level", gameLevel.getLevelName());

        gameLevel.setLevelName("The worst level");
        assertEquals("The worst level", gameLevel.getLevelName());

        gameLevel.setLevelName("A good level");
        assertEquals("A good level", gameLevel.getLevelName());
    }

    /**
     * Test 3
     * Checking the setLowNumber method works properly or not
     */
    @Test
    void setLowNumber() {
        assertThrows(IllegalArgumentException.class, () -> gameLevel.setLowNumber(-1));

        gameLevel.setHighNumber(4);
        assertThrows(LevelException.class, () -> gameLevel.setLowNumber(5));

        gameLevel.setLowNumber(2);
        assertEquals(2, gameLevel.getLowNumber());

        gameLevel.setLowNumber(3);
        assertEquals(3, gameLevel.getLowNumber());
    }

    /**
     * Test 4
     * Checks the setHighNumber method works well or not
     */
    @Test
    void setHighNumber() {
        assertThrows(LevelException.class, () -> gameLevel.setHighNumber(-1));

        gameLevel.setHighNumber(2);
        assertEquals(2, gameLevel.getHighNumber());

        gameLevel.setHighNumber(3);
        assertEquals(3, gameLevel.getHighNumber());

        gameLevel.setHighNumber(4);
        assertEquals(4, gameLevel.getHighNumber());
    }

    /**
     * Test 5
     * Checks the setNumOfDigits methods works fine or not
     */
    @Test
    void setNumOfDigits() {
        assertThrows(LevelException.class, () -> gameLevel.setNumOfDigits(0));

        gameLevel.setNumOfDigits(1);
        assertEquals(1, gameLevel.getNumOfDigits());

        gameLevel.setNumOfDigits(2);
        assertEquals(2, gameLevel.getNumOfDigits());

        gameLevel.setNumOfDigits(3);
        assertEquals(3, gameLevel.getNumOfDigits());
    }

    /**
     * Test 6
     * Checks the setAllowDoubles method works properly or not
     */
    @Test
    void setAllowDoubles() {
        assertFalse(gameLevel.isAllowDoubles());

        gameLevel.setAllowDoubles(true);
        assertTrue(gameLevel.isAllowDoubles());
    }
}