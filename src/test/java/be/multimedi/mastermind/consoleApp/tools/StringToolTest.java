package be.multimedi.mastermind.consoleApp.tools;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import static org.junit.jupiter.api.Assertions.*;

class StringToolTest {

   @Test
   protected void testSetXValidation() {

      assertAll(
              () -> assertEquals("12x", StringTool.setX("123", 2)),
              () -> assertEquals("1x345", StringTool.setX("12345", 1)),
              () -> assertEquals("1234", StringTool.setX("1234", -1)),
              () -> assertNull(null, StringTool.setX(null, 1)),
              () -> assertEquals("x", StringTool.setX("x", 1)),
              () -> assertEquals("1x3xx", StringTool.setX("123xx", 1)),
              () -> assertEquals("1xK4", StringTool.setX("12K4", 1)),
              () -> assertEquals("1x34", StringTool.setX("1x34", 4))
      );
   }

   @ParameterizedTest(name = "Input -> {0}, Index -> {1}, Output -> {2}")
   @CsvFileSource(resources="/xestest.csv", numLinesToSkip = 1)
   protected void testSetXValues(String input, int index, String output) {
      assertEquals(output, StringTool.setX(input, index));
   }
}