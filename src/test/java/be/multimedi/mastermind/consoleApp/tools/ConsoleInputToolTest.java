package be.multimedi.mastermind.consoleApp.tools;

import be.multimedi.mastermind.testTools.SystemInOutTester;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ConsoleInputToolTest extends SystemInOutTester {

   private ConsoleInputTool ct;
   static final String DEFAULT_QUESTION_Y_N = "Question";
   static final String TESTING_INPUT = "Testing";

   @BeforeEach
   protected void init() {
      ct = new ConsoleInputTool();
   }

   @Test
   protected void testAskPressEnterToContinue() {
      setInput("");
      ct.askPressEnterToContinue();
      assertEquals("Press enter to continue.", getOutput());
   }

   @Test
   protected void testAskYesOrNoValidation() {
      assertFalse(ct.askYesOrNo(null));
      assertFalse(ct.askYesOrNo(""));
   }

   @Test
   protected void testAskYesOrNoValues() {

      // input: "y"
      setInput("y");
      assertTrue(ct.askYesOrNo(DEFAULT_QUESTION_Y_N));
      assertTrue(getOutput().contains(DEFAULT_QUESTION_Y_N));
      assertTrue(getError().isBlank());

      // input: "n"
      resetStreams();
      setInput("n");
      assertFalse(ct.askYesOrNo(DEFAULT_QUESTION_Y_N));
      assertTrue(getOutput().contains(DEFAULT_QUESTION_Y_N));
      assertTrue(getError().isBlank());

      // input: "Y"
      resetStreams();
      setInput("Y");
      assertTrue(ct.askYesOrNo(DEFAULT_QUESTION_Y_N));
      assertTrue(getOutput().contains(DEFAULT_QUESTION_Y_N));
      assertTrue(getError().isBlank());

      // input: "n"
      resetStreams();
      setInput("N");
      assertFalse(ct.askYesOrNo(DEFAULT_QUESTION_Y_N));
      assertTrue(getOutput().contains(DEFAULT_QUESTION_Y_N));
      assertTrue(getError().isBlank());

      // input: "J"
      resetStreams();
      setInput("J");
      addInputLine("y");
      assertTrue(ct.askYesOrNo(DEFAULT_QUESTION_Y_N));
      assertTrue(getOutput().contains(DEFAULT_QUESTION_Y_N));
      assertEquals("Type in y or n", getError().replace("\n", ""));

      // input: "no"
      resetStreams();
      setInput("no");
      addInputLine("n");
      assertFalse(ct.askYesOrNo(DEFAULT_QUESTION_Y_N));
      assertTrue(getOutput().contains(DEFAULT_QUESTION_Y_N));
      assertEquals("Answer cannot be longer than 1 charType in y or n", getError().replace("\n", ""));
   }

   @Test
   protected void testAskUserPosIntBetweenRange() {
      // validation tests
      assertThrows(IllegalArgumentException.class, () -> ct.askUserPosIntBetweenRange("Int between 9 and 5", 9, 5));
      assertThrows(IllegalArgumentException.class, () -> ct.askUserPosIntBetweenRange(null, 5, 9));
      assertThrows(IllegalArgumentException.class, () -> ct.askUserPosIntBetweenRange("Int between -5 and 6", -5, 6));

      // functionality tests
      resetStreams();
      setInput("2");
      addInputLine("5");
      ct.askUserPosIntBetweenRange("Int between 3 and 6", 3, 6);
      assertEquals("Input must be a number between 3 and 6", getError().replace("\n", ""));

      resetStreams();
      setInput("5");
      assertEquals(5, ct.askUserPosIntBetweenRange("Int between 3 and 6", 3, 6));

      resetStreams();
      setInput("6");
      assertEquals(6, ct.askUserPosIntBetweenRange("Int between 6 and 7", 6, 7));
   }

   @Test
   protected void testAskSpecificAmountOfPosDigitsStringBetweenRangeValidation() {
      assertNull(ct.askSpecificAmountOfPosDigitsStringBetweenRange(TESTING_INPUT, 0, 1, 9));
      assertNull(ct.askSpecificAmountOfPosDigitsStringBetweenRange(TESTING_INPUT, 6, 6, 3));
      assertNull(ct.askSpecificAmountOfPosDigitsStringBetweenRange(TESTING_INPUT, 4, -1, 5));
      assertNull(ct.askSpecificAmountOfPosDigitsStringBetweenRange(TESTING_INPUT, 4, -5, -2));
      assertNull(ct.askSpecificAmountOfPosDigitsStringBetweenRange(TESTING_INPUT, 6, 10, 12));
      assertNull(ct.askSpecificAmountOfPosDigitsStringBetweenRange(TESTING_INPUT, 5, 8, 15));
   }

   @Test
   protected void testAskSpecificAmountOfPosDigitsStringBetweenRangeValues() {

      // input: "1234", length: 4, from: 1, to: 6
      setInput("1234");
      assertEquals("1234", ct.askSpecificAmountOfPosDigitsStringBetweenRange("Four digits between 1-6", 4, 1, 6));
      assertTrue(getError().isBlank());

      // input: "34553", length: 5, from: 3, to: 5
      resetStreams();
      setInput("34553");
      assertEquals("34553", ct.askSpecificAmountOfPosDigitsStringBetweenRange("Five digits between 3-5", 5, 3, 5));
      assertTrue(getError().isBlank());

      // input: "345" & "34554", length: 5, from: 3, to: 5
      resetStreams();
      setInput("345");
      addInputLine("34554");
      assertEquals("34554",
              ct.askSpecificAmountOfPosDigitsStringBetweenRange("Five digits between 3-5", 5, 3, 5));
      assertEquals("Type in exactly 5 digits between 3 and 5", getError().replace("\n", ""));

      // input: "012356789" & "0123456789", length: 10, from: 0, to: 9
      resetStreams();
      setInput("012356789");
      addInputLine("0123456789");
      assertEquals("0123456789",
              ct.askSpecificAmountOfPosDigitsStringBetweenRange("Ten digits between 0-9", 10, 0, 9));
      assertEquals("Type in exactly 10 digits between 0 and 9", getError().replace("\n", ""));

      // input: "-123" & "123", length: 3, from: 1, to: 3
      resetStreams();
      setInput("-123");
      addInputLine("123");
      assertEquals("123",
              ct.askSpecificAmountOfPosDigitsStringBetweenRange("Three digits between 1-3", 3, 1, 3));
      assertEquals("Type in exactly 3 digits between 1 and 3", getError().replace("\n", ""));
   }

   @Test
   protected void testCheckNoDoublesInString() {

      // input: "1234"
      resetStreams();
      assertTrue(ct.checkNoDoublesInString("1234"));
      assertTrue(getError().isBlank());

      // input: "Aa"
      resetStreams();
      assertTrue(ct.checkNoDoublesInString("Aa"));
      assertTrue(getError().isBlank());

      // input: "0123456789abc"
      resetStreams();
      assertTrue(ct.checkNoDoublesInString("0123456789abc"));
      assertTrue(getError().isBlank());

      // input: "11"
      resetStreams();
      assertFalse(ct.checkNoDoublesInString("11"));
      assertEquals("The input contains doubles", getError().replace("\n", ""));

      // input: "AA"
      resetStreams();
      assertFalse(ct.checkNoDoublesInString("AA"));
      assertEquals("The input contains doubles", getError().replace("\n", ""));

      // input: "Aaa"
      resetStreams();
      assertFalse(ct.checkNoDoublesInString("Aaa"));
      assertEquals("The input contains doubles", getError().replace("\n", ""));

      // input: "1a23569a52"
      resetStreams();
      assertFalse(ct.checkNoDoublesInString("1a23569a52"));
      assertEquals("The input contains doubles", getError().replace("\n", ""));

      // input: "01234561987"
      resetStreams();
      assertFalse(ct.checkNoDoublesInString("01234561987"));
      assertEquals("The input contains doubles", getError().replace("\n", ""));

      // input: null
      resetStreams();
      assertFalse(ct.checkNoDoublesInString(null));
      assertTrue(getError().isBlank());

      // input: ""
      resetStreams();
      assertFalse(ct.checkNoDoublesInString(""));
      assertTrue(getError().isBlank());
   }
}